Submission scripts to run Rockstar, ConsistentTrees, or assign particle species to halos. 
We recommend copying the scripts to your local folder, making any necessary edits (for example, to the run name), and submitting from there.

A detailed tutorial for generating halo catalogs and merger trees via Rockstar-Galaxies and ConsistentTrees is here:

https://sites.google.com/view/wetzelgroup/computing/generating-halo-catalogs?authuser=0


##### Running on Stampede2

The following steps will refer to this directory as `$RSDIR`

1. Set up your environment. The scripts assume that all the executables will be in your home directory: `$HOME/local/halo`. You can put them elsewhere, but you will have to change the `halo_directory` variable in several of the following scripts. Clone the forked versions of `rockstar-galaxies` (https://bitbucket.org/awetzel/rockstar-galaxies) and `consistent-trees` into that directory, and run `make clean; make` in each.

2. Create your directory structure in the simulation directory:

    cd /path/to/simulation/directory
    mkdir -p halo/rockstar_dm/
    cd halo/rockstar_dm
    mkdir catalog rockstar_jobs

3. Set up and run rockstar on all of the snapshots, which should be as simple as:

    cp $RSDIR/snapshot_indices.txt .
    cp $RSDIR/[cluster_name]/submit_rockstar.py .

where <cluster_name> is `stampede`, for example, then edit `submit_rockstar.py` to change `mail-user`, and potentially also `account` and `halo_directory`, then submitting the script (making sure that the currently active `python` can find the `utilities` package). If your job runs out of time, you should be able to just resubmit the same script -- it looks for `catalog/restart.cfg` and uses it if it exists.

4. Build the trees and create the hlists. This unfortunately takes 2 steps, because the configuration file that the script creates will be slightly wrong. Therefore, you need to create the configuration file:

    perl $HOME/local/halo/rockstar-galaxies/scripts/gen_merger_cfg.pl catalog/rockstar.cfg
Then edit the configuration file as follows:

    a. comment out (with a #) the line named SNAPSHOT_NAMES
    b. change NUM_SNAPS to = 500 (for default FIRE-3)
    c. set STARTING_SNAP to the first out_xxx.list with any halos (for example, 5)

Finally, copy `$RSDIR/submit_tree.py` to the working directory, set `mail-user` and `account` as necessary, make sure that the line that generates the merger tree config file is commented out (since we already did that), and submit the script.

5. Convert the `out_xxx.list`, `hlist`, and `tree`  files to HDF5. This should be as simple as copying over `$RSDIR/<cluster_name>/submit_hdf5.py`, changing the same 2 SBATCH options as before, making sure that `halo_analysis` is accessible to the active `python`, and submitting the script.

6. Add star particle properties (create the `star_xxx.hdf5` files). Again, this should be as simple as copying over `$RSDIR/<cluster_name>/submit_particle.py`, changing the SBATCH options, making sure that `halo_analysis` and `gizmo_analysis` are accessible to the active `python`, and submitting.


##### Running on Rusty (Flatiron)

Perform the steps above, but use replace `_stampede.py` with `_rusty.py` and copy scripts from $RSDIR/rusty. Some notes:

1. I had difficulty getting the run going again once it ran out of time, because `rockstar` kept saying that it was getting connections refused and so on. My first bit of advice if you hit this problem is to be patient, because it might start working again (give it 5 to 10 minutes at least). If that fails, I managed to get it going by setting the following in `restart.cfg` (but make a backup of the original that `rockstar` creates first!):

    PARALLEL_IO_SERVER_ADDRESS = "auto"
    PARALLEL_IO_SERVER_PORT = "auto"
    PARALLEL_IO_WRITER_PORT = 30000
    PARALLEL_IO_SERVER_INTERFACE = ""

2. I had trouble getting the `hlist` script going, because Perl could not find the submodules that live in `consistent-trees/src`. I solved this problem by deleting the second line (`#use lib qw(src);`) and replacing it with

    use FindBin;                     # locate this script
    use lib "$FindBin::RealBin/src";  # use the location of this script + "/src"


##### Rnning AHF (followed by consistent-trees, HDF5 conversion, and particle assignment) on Rusty

1. Copy `submit_ahf_analysis_rusty.py` to your working directory and edit the top of the script (i.e. to define variables and paths). Nothing below the definition of `checkdir` should need to be changed.  Obviously every variable needs to be correct, but you should be most aware of the `home` directory, which sets where the code looks for `ahf` and `consistent-trees`, and their template parameter files `AHF.input`, and `ctrees-input.cfg`.

2. Ensure that you have the template parameter files in the directory pointed to by `home`. The only varaibles that will be changed in `AHF.input` are `ic_filename`, `outfile_prefix`, and `ic_filetype`, so these lines can be set to dummy values (but must exist!).  For the `consistent-trees` input file, the script will overwrite all of the paths, the cosmological parameters, and the box size.

Once everything is set up, **run** (do *not* submit) it with an argument of "ahf". The script will:

1. create an parameter file for each snapshot (using the template in the `AHF.input` file located in `home`
2. create a file with the commands needed to run AHF on each of those snapshots in turn
3. create a SLURM script to run those commands using `disBatch`, with as many tasks as granted in the header of the file going in parallel (note each task is entirely independent)
4. submit the SLURM script.

The final step of that SLURM job (assuming it finishes successfully) will resubmit itself with an argument of "mtree", which will set up and run the AHF particle-based MergerTree as follows:

1. Create a file with each of the snapshot(i) -> snapshot(i+1) MergerTree commands
2. Create a SLURM script to use `disBatch` to run that list of commands

The final step of _that_ SLURM script will again call `submit_ahf_analysis_rusty.py`, this time with an argument of 'ctrees', which will:

1. Create the consistent trees input parameter file using the template provided
2. Create and submit a SLURM script that will (a) convert the `.AHF_halos` and `_mtree_idx` files into the `out_xxx.list` files that `consistent-trees` expects, (b) run `consistent-trees`, and (c) run `halo_trees_to_catalog.pl` to create the `hlist` files.

At this point, you should be able to pick up the rest of the conversion/assignment/analysis with `submit_hdf5_rusty.py` and `submit_particle_rusty.py`

Note that at each step, the script will check for existing files and skip those snapshots.
