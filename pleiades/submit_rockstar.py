#!/usr/bin/env python3

# job name
#PBS -N rockstar
# queue: devel <= 2 hr, normal <= 8 hr, long <= 5 day
##PBS -q devel
##PBS -q normal
#PBS -q long
# cpu configuration
# select = nodes; ncpus = cpus per node; mpiprocs, ompthreads = mpi tasks, openmp threads per node
# Sandy Bridge (node has 16 cores, 2 GB per core, 32 GB total)
##PBS -l select=1:ncpus=16:mpiprocs=9:ompthreads=1:model=san
# Ivy Bridge (node has 20 cores, 3.2 GB per core, 64 GB total)
#PBS -l select=1:ncpus=20:mpiprocs=9:ompthreads=1:model=ivy
# Haswell (node has 24 cores, 5.3 GB per core, 128 GB total)
##PBS -l select=1:ncpus=24:mpiprocs=9:ompthreads=1:model=has
#PBS -l walltime=48:00:00
# combine stderr & stdout into one file
#PBS -j oe
# output file name
##PBS -o rockstar_jobs/rockstar_job_$PBS_JOBID.txt
# email results: a = aborted, b = begin, e = end
#PBS -M arwetzel@gmail.com
#PBS -m bae
# import terminal environmental variables
#PBS -V
# account to charge
#PBS -W group_list=s2355
##PBS -W group_list=s2445
##PBS -W group_list=s2566

'''
Submit job to run Rockstar halo finder.
Submit this script from within the rockstar sub-directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import glob

from utilities import io as ut_io
from gizmo_analysis import gizmo_io

# parallelization parameters
node_number = 1  # number of nodes
mpi_number_per_node = 9  # number of MPI tasks per node

# names of files and directories
halo_directory = os.environ['HOME'] + '/local/halo/rockstar-galaxies/'
executable_file_name = halo_directory + 'rockstar-galaxies'
catalog_directory = 'catalog/'
config_file_name_restart = 'restart.cfg'

# set number of file blocks per snapshot (if None, try to read from snapshot header)
snapshot_block_number = None

# move to directory am in when submit this job
os.chdir(os.environ['PBS_O_WORKDIR'])

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('pbs', node_number, mpi_number_per_node)

# check if restart config file exists - if so, initiate restart job
config_file_name_restart = glob.glob(catalog_directory + config_file_name_restart)

if len(config_file_name_restart) > 0:
    config_file_name = config_file_name_restart[0]
else:
    if not snapshot_block_number:
        # read number of snapshot blocks from snapshot header
        simulation_directory = '../../.'  # relative path of base simulation directory
        try:
            header = gizmo_io.Read.read_header(
                simulation_directory, snapshot_value_kind='index', snapshot_value=500
            )
        except OSError:
            header = gizmo_io.Read.read_header(
                simulation_directory, snapshot_value_kind='index', snapshot_value=20
            )
        except OSError:
            header = gizmo_io.Read.read_header(
                simulation_directory, snapshot_value_kind='index', snapshot_value=0
            )
        except OSError:
            print(f'! cannot read snapshot file in {simulation_directory} to get file block count')
        snapshot_block_number = header['file.number.per.snapshot']

    if snapshot_block_number == 1:
        config_file_name = 'rockstar_config.txt'
    else:
        config_file_name = f'rockstar_config_blocks{snapshot_block_number}.txt'

    config_file_name = halo_directory + config_file_name

# start server
os.system(f'{executable_file_name} -c {config_file_name} &')

# start worker
os.system(
    f'mpiexec -np {SubmissionScript.mpi_number} {executable_file_name}'
    + f' -c {catalog_directory}auto-rockstar.cfg'
    + ' >> rockstar_jobs/rockstar_log.txt'
)

SubmissionScript.print_runtime()
