#!/usr/bin/env python3

# Frontera node: 56 cores, 3.4 GB per core, 192 GB total
#SBATCH --job-name=consistent_tree
##SBATCH --partition=development  # 2 hours, 1-40 nodes, 1 job
#SBATCH --partition=small  # 2 days, 2-24 nodes, 20 jobs
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    ## processes per node
##SBATCH --cpus-per-task=1    ## processes per task
#SBATCH --time=48:00:00
#SBATCH --output=rockstar_jobs/consistent_tree_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=AST21010

'''
Submit job to run ConsistentTrees halo merger trees.
Submit this script from within the rockstar sub-directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>

Steps
(1) modify catalog/rockstar.cfg to ensure:
    STARTING_SNAP = 1 (or the first snapshot with any halos)
    NUM_SNAPS = 500
    #SNAPSHOT_NAMES = "snapshot_indices.txt"
(2) run: generate-tree-file
    (alias for perl ~/local/halo/rockstar-galaxies/scripts/gen_merger_cfg.pl catalog/rockstar.cfg)
    to generate rockstar config file in: catalog/outputs/merger_tree.cfg
(3) modify catalog/outputs/merger_tree.cfg (if necessary) to ensure:
    BOX_DIVISIONS=1
(4) run ConsistentTrees
'''

import os

from utilities import io as ut_io

# directories and files
halo_directory = os.environ['HOME'] + '/local/halo/'
rockstar_directory = halo_directory + 'rockstar-galaxies'
consistentrees_directory = halo_directory + 'consistent-trees'
tree_config_file = 'catalog/outputs/merger_tree.cfg'

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# generate merger tree config file (catalog/outputs/merger_tree.cfg) from rockstar config file
# os.system(f'perl {rockstar_directory}/scripts/gen_merger_cfg.pl catalog/rockstar.cfg')

# generate tree files
# assume non-periodic boundaries
os.system(
    f'perl {consistentrees_directory}/do_merger_tree_np.pl'
    + f' {consistentrees_directory} {tree_config_file}'
)
# assume periodic boundaries
# os.system('perl {}/do_merger_tree.pl {} {}'.format(
#    consistentrees_directory, consistentrees_directory, tree_config_file))

# generate halo progenitor (hlist) catalogs from trees
os.system(
    f'perl {consistentrees_directory}/halo_trees_to_catalog.pl'
    + f' {consistentrees_directory} {tree_config_file}'
)

# print run-time information
ScriptPrint.print_runtime()
