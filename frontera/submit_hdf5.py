#!/usr/bin/env python3

# Frontera node: 56 cores, 3.4 GB per core, 192 GB total
#SBATCH --job-name=rockstar_hdf5
##SBATCH --partition=development  # 2 hours, 1-40 nodes, 1 job
#SBATCH --partition=small  # 2 days, 2-24 nodes, 20 jobs
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    # MPI tasks per node
#SBATCH --cpus-per-task=1    # processes per MPI task
#SBATCH --time=8:00:00
#SBATCH --output=rockstar_jobs/rockstar_hdf5_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=AST21010

'''
Submit job to convert halo text files to HDF5 format.
Submit this script from within the rockstar sub-directory of the simulation.
The creation of HDF5 files does not support parallelization.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os

from utilities import io as ut_io
from halo_analysis import halo_io

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# assume am in rockstar directory
current_directory = os.getcwd().split('/')
rockstar_directory = current_directory[-2] + '/' + current_directory[-1]

halo_io.IO.rewrite_as_hdf5('../../', rockstar_directory)

# print run-time information
ScriptPrint.print_runtime()
