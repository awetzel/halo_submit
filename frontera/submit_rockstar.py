#!/usr/bin/env python3

# Frontera node: 56 cores, 3.4 GB per core, 192 GB total
#SBATCH --job-name=rockstar
##SBATCH --partition=development  # 2 hours, 1-40 nodes, 1 job
#SBATCH --partition=small  # 2 days, 2-24 nodes, 20 jobs
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=9    # processes per node
#SBATCH --time=48:00:00
#SBATCH --output=rockstar_jobs/rockstar_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=AST21010

'''
Submit job to run Rockstar halo finder.
Submit this script from within the rockstar sub-directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import glob

from utilities import io as ut_io
from gizmo_analysis import gizmo_io

# names of files and directories
halo_directory = os.environ['HOME'] + '/local/halo/rockstar-galaxies/'
executable_file_name = halo_directory + 'rockstar-galaxies'
catalog_directory = 'catalog/'
config_file_name_restart = 'restart.cfg'

# set number of file blocks per snapshot (if None, try to read from snapshot header)
snapshot_block_number = None

os.system('umask 0022')  # set permission of files created to be read-only for group and other

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('slurm')

# check if restart config file exists - if so, initiate restart job
config_file_name_restart = glob.glob(catalog_directory + config_file_name_restart)

if len(config_file_name_restart) > 0:
    config_file_name = config_file_name_restart[0]
else:
    if not snapshot_block_number:
        # read number of snapshot blocks from snapshot header
        simulation_directory = '../../.'  # relative path of base simulation directory
        try:
            header = gizmo_io.Read.read_header(
                simulation_directory, snapshot_value_kind='index', snapshot_value=500
            )
        except OSError:
            header = gizmo_io.Read.read_header(
                simulation_directory, snapshot_value_kind='index', snapshot_value=20
            )
        except OSError:
            header = gizmo_io.Read.read_header(
                simulation_directory, snapshot_value_kind='index', snapshot_value=0
            )
        except OSError:
            print(f'! cannot read snapshot file in {simulation_directory} to get file block count')
        snapshot_block_number = header['file.number.per.snapshot']

    if snapshot_block_number == 1:
        config_file_name = 'rockstar_config.txt'
    else:
        config_file_name = f'rockstar_config_blocks{snapshot_block_number}.txt'

    config_file_name = halo_directory + config_file_name

# start server
os.system(f'{executable_file_name} -c {config_file_name} &')

# start worker
os.system(
    f'ibrun mem_affinity {executable_file_name} -c {catalog_directory}auto-rockstar.cfg'
    + ' >> rockstar_jobs/rockstar_log.txt'
)

SubmissionScript.print_runtime()
