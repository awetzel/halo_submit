#!/usr/bin/env python3

# Frontera node: 56 cores, 3.4 GB per core, 192 GB total
#SBATCH --job-name=rockstar_particle
##SBATCH --partition=development  # 2 hours, 1-40 nodes, 1 job
#SBATCH --partition=small  # 2 days, 2-24 nodes, 20 jobs
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    # MPI tasks per node
#SBATCH --cpus-per-task=12    # processes per MPI task
#SBATCH --time=48:00:00
#SBATCH --output=rockstar_jobs/rockstar_particle_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=AST21010

'''
Submit job to assign particles to halos.
Submit this script from within the rockstar sub-directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import numpy as np

from utilities import io as ut_io
from halo_analysis import halo_io
from halo_analysis import halo_default

species_name = 'star'  # particle species to assign
snapshot_value_kind = 'index'  # how to select snapshot
snapshot_values = 'all'  # which snapshots to generate for

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# check if input arguments
if len(os.sys.argv) > 1:
    snapshot_selection = str(os.sys.argv[1])

assert snapshot_values in ['single', 'subset', 'all']
# 'single' = single snapshot
# 'all' = all snapshots with halos
# 'subset' = default subset list of 64 snapshots

if snapshot_values == 'single':
    # run on single snapshot
    if len(os.sys.argv) > 2:
        snapshot_values = int(os.sys.argv[2])
elif snapshot_values == 'subset':
    snapshot_values = halo_default.snapshot_indices_subset
elif snapshot_values == 'all':
    if len(os.sys.argv) > 2:
        snapshot_index_min = int(os.sys.argv[2])
        snapshot_index_max = 600
        if len(os.sys.argv) > 3:
            snapshot_index_max = int(os.sys.argv[3])
        snapshot_values = np.arange(snapshot_index_min, snapshot_index_max + 1)

print(f'assigning {species_name} particles to halos at {snapshot_value_kind}[s]: {snapshot_values}')
os.sys.stdout.flush()

Particle = halo_io.ParticleClass()

Particle.write_catalogs_with_species(
    species_name, snapshot_value_kind, snapshot_values, proc_number=ScriptPrint.omp_number
)

# print run-time information
ScriptPrint.print_runtime()
