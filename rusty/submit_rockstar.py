#!/usr/bin/env python3

# 28-core Broadwell nodes with 512GB of RAM or
# 40-core Skylake nodes with 768GB of RAM.
# set --constraint=skylake to get latter
#SBATCH --job-name=rockstar
#SBATCH --partition=cca
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=9    ## MPI tasks per node
#SBATCH --time=168:00:00
#SBATCH --output=rockstar_jobs/rockstar_job_%j.txt
#SBATCH --mail-user=sheagk@gmail.com
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --mail-type=begin
#SBATCH --exclusive

'''
Submit job to run Rockstar halo finder.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import glob

from utilities import io as ut_io
from gizmo_analysis import gizmo_io

# names of files and directories
halo_directory = os.environ['HOME'] + '/local/halo/rockstar-galaxies_dm/'
executable_file_name = halo_directory + 'rockstar-galaxies'
catalog_directory = 'catalog/'
config_file_name_restart = 'restart.cfg'

os.system('umask 0022')  # set permission of files created to be read-only for group and other

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('slurm')

# check if restart config file exists - if so, initiate restart job
config_file_name_restart = glob.glob(catalog_directory + config_file_name_restart)

if len(config_file_name_restart) > 0:
    config_file_name = config_file_name_restart[0]
else:
    # set number of file blocks per snapshot, or read from snapshot header
    snapshot_block_number = None
    if not snapshot_block_number:
        simulation_directory = '../../.'
        try:
            header = gizmo_io.Read.read_header(
                simulation_directory, snapshot_value_kind='index', snapshot_value=500
            )
        except OSError:
            header = gizmo_io.Read.read_header(
                simulation_directory, snapshot_value_kind='index', snapshot_value=20
            )
        except OSError:
            header = gizmo_io.Read.read_header(
                simulation_directory, snapshot_value_kind='index', snapshot_value=0
            )
        except OSError:
            print(f'! cannot read snapshot file in {simulation_directory} to get file block count')
        snapshot_block_number = header['file.number.per.snapshot']

    if snapshot_block_number == 1:
        config_file_name = 'rockstar_config.txt'
    else:
        config_file_name = f'rockstar_config_blocks{snapshot_block_number}.txt'

    config_file_name = halo_directory + config_file_name

# start server
os.system(f'{executable_file_name} -c {config_file_name} &')

# start worker
os.system(
    'srun {} -c {} >> rockstar_jobs/rockstar_log.txt'.format(
        executable_file_name, catalog_directory + 'auto-rockstar.cfg'
    )
)

SubmissionScript.print_runtime()
