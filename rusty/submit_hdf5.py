#!/usr/bin/env python3

#SBATCH --job-name=rockstar_hdf5
#SBATCH --partition=cca    ## KNL node: 64 cores x 2 FP threads, 1.6 GB per core, 96 GB total
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    ## MPI tasks per node
#SBATCH --cpus-per-task=1    ## OpenMP threads per MPI task
#SBATCH --exclusive
#SBATCH --output=rockstar_jobs/rockstar_hdf5_job_%j.txt
#SBATCH --mail-user=sheagk@gmail.com
#SBATCH --mail-type=fail
#SBATCH --mail-type=end

'''
Submit job to convert halo text files to HDF5 format.
Submit this script from within the rockstar halo sub-directory of the simulation.
The creation of HDF5 files does not support parallelization.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os

from utilities import io as ut_io
from halo_analysis import halo_io

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# assume am in rockstar directory
current_directory = os.getcwd().split('/')
rockstar_directory = current_directory[-2] + '/' + current_directory[-1]

halo_io.IO.rewrite_as_hdf5('../../', rockstar_directory)

# print run-time information
ScriptPrint.print_runtime()
