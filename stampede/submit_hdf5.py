#!/usr/bin/env python3

# Stampede3
# SPR node: 112 cores, 128 GB
# ICX node: 80 cores, 256 GB, 3.2 (3.0 useable) GB per core
# SKX node: 48 cores, 192 GB, 4.0 (3.5 useable) GB per core
#SBATCH --job-name=rockstar_hdf5
#SBATCH --partition=icx
##SBATCH --partition=skx
##SBATCH --partition=skx-dev
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    # MPI tasks per node
#SBATCH --cpus-per-task=1    # processes per MPI task
#SBATCH --time=8:00:00
#SBATCH --output=rockstar_jobs/rockstar_hdf5_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=TG-PHY240075

'''
Submit job to convert halo text files to HDF5 format.
Submit this script from within the rockstar sub-directory of the simulation.
The creation of HDF5 files does not support parallelization.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os

from utilities import io as ut_io
from halo_analysis import halo_io

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# assume am in rockstar directory
current_directory = os.getcwd().split('/')
rockstar_directory = current_directory[-2] + '/' + current_directory[-1]

halo_io.IO.rewrite_as_hdf5('../../', rockstar_directory)

# print run-time information
ScriptPrint.print_runtime()
